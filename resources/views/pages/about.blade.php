<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{$appName}} | Home</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">    

    </head>

    <body class="bg-light">

    @include('inc.navbar')


      <section>
          <div class="container p-5">
              <h1>About Us</h1>
          </div>
      </section>

     

       <div style="height: 200px"></div>
     <!-- Scripts -->
     <script src="{{ mix('js/app.js') }}" defer></script>
    </body>
</html>

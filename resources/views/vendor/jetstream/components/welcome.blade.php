<div class="row justify-content-center my-5 pt-4">
    <div class="col-md-12">

      
        <div class="card shadow bg-light mt-5">
            <div class="card-body bg-white px-5 py-3 border-bottom rounded-top">

                <h3 class="h3 my-4 font-weight-bold text-muted">
                    Welcome to your {{$appName}} Dashboard
                </h3>

                <div class="card-body">
  

                    @can('isAdmin')
                    <strong>You have Admin Access</strong>

                        <div class="alert alert-success p-4">
                           <img class="rounded-circle" width="100" height="100" src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->name }}" />
  
                            <div class="mt-4 bg-white">
                             
                              <table class="table">
                                <thead>
                                  <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Username</th>
                                    <th scope="col">Email</th>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                  </tr>
                                </thead>
                                <tbody>
                                
                                 <tr>
                                  <th scope="row">1</th>
                                  <td></td>
                                  <td>Otto</td>
                                  <td>mark@mail.com</td>
                                  <td>
                                  <td><a href="#0" class="btn btn-primary btn-sm">Veiw details</a></td>
                                  </td>
                                  <td><a href="#0" class="btn btn-danger btn-sm">Delete user</a></td>
                                </tr>
                                </tbody>
                              </table>

                            </div>

                        </div>


                    @endcan

                    @can('isUser')
                          <strong>You have User Access</strong>

                        <div class="alert alert-success">

                          <img class="rounded-circle" width="100" height="100" src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->name }}" />
                 
                          <section class="p-3">
                              <div class="container">
                                  <div class="row">
          
                                      <div class="col-lg-6 col-md-6 col-sm-12">
                                          <div class="list-group mt-2 bg-white">
                                              <a type="button" class="list-group-item list-group-item-action active" aria-current="true">
                                                ACCOUNT DETAILS
                                              </a>
                                              <a type="button" class="list-group-item list-group-item-action">
                                                 <strong>USERNAME:</strong> {{Auth::user()->username}}
                                              </a>
                                              <a type="button" class="list-group-item list-group-item-action">
                                                 <strong>EMAIL:</strong> {{Auth::user()->email}}
                                              {{-- </a>
                                              <a type="button" class="list-group-item list-group-item-action">
                                                  <strong>REFERRER NAME:</strong> {{ Auth::user()->referrer->name ?? 'Not Specified' }}
                                               </a>
                                               <a type="button" class="list-group-item list-group-item-action">
                                                  <strong>REFERRALS</strong> {{ count(Auth::user()->referrals)  ?? '0' }}
                                               </a> --}}
                                              <a type="button" class="list-group-item list-group-item-action">
                                                  <strong>REFERRAL LINK:</strong> {{ Auth::user()->referral_link }}
                                              </a>
                                              <a type="button" class="list-group-item list-group-item-action" disabled>
                                                  <strong>CREATE AT:</strong> {{Auth::user()->created_at}}
                                               </a>
                                            </div>
                                      </div>
          
                                      <div class="col-lg-6 col-md-6 col-sm-12">
                                          <ul class="list-group mt-2 bg-white">
                                              <li class="list-group-item d-flex justify-content-between align-items-center">
                                                A list item
                                                <span class="badge badge-primary badge-pill">14</span>
                                              </li>
                                              <li class="list-group-item d-flex justify-content-between align-items-center">
                                                A second list item
                                                <span class="badge badge-primary badge-pill">2</span>
                                              </li>
                                              <li class="list-group-item d-flex justify-content-between align-items-center">
                                                A third list item
                                                <span class="badge badge-primary badge-pill">1</span>
                                              </li>
                                              <li class="list-group-item d-flex justify-content-between align-items-center">
                                                  A list item
                                                  <span class="badge badge-primary badge-pill">54</span>
                                                </li>
                                            </ul>
                                                 <div class="py-2 px-1">
                                                     <a href="#" class="btn btn-primary my-1 mx-1">Activate Premium</a>
                                                     <a href="#" class="btn btn-warning my-1 mx-1">Activate Gold</a>
                                                 </div>
                                      </div>
          
                                  </div>
                              </div>
                          </section>
                         

                        </div>
                       @endcan

                </div>
                <hr>


            </div>  
            </div>
        </div>
    </div>
</div>



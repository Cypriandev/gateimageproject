<x-app-layout>
    <x-slot name="header">
        <span class"">
           <span class="font-weight-bold h4"> {{ __('Dashboard') }} </span> 
           <span class="p">{{Auth::user()->email}}</span>
        </span>
    </x-slot>

    <x-jet-welcome />
</x-app-layout>
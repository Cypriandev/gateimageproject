<nav class="navbar navbar-expand-lg navbar-light bg-white shadow-sm">
    <div class="container">
        <a class="navbar-brand font-weight-bold" href="{{url('/')}}"><img src="img/logo.png" height="30" alt=""></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- left navigation-->
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="{{url('/')}}">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{url('about')}}">About</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Dropdown
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li>
      </ul>

      {{-- center navigation --}}
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#0"><img src="img/facebook.png" height="20" alt=""></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#0"><img src="img/twitter.png" height="20" alt=""></a> 
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#0"><img src="img/instagram.png" height="20" alt=""></a> 
        </li>
      </ul>

        <!-- right navigation-->
        @if (Route::has('login'))
        <ul class="navbar-nav">
          <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-dark my-2 my-sm-0" type="submit">Search</button>
          </form>
            @auth
            <li class="nav-item">
              <a class="nav-link btn btn-dark btn-sm text-light mx-1 my-1" href="{{url('/dashboard')}}" tabindex="-1" aria-disabled="true">Dashboard</a>
            </li>
            @else
            <li class="nav-item">
                <a class="nav-link btn btn-dark btn-sm text-light mx-1 my-1" href="{{route('login')}}" tabindex="-1" aria-disabled="true">Login</a>
              </li>
              @if (Route::has('register'))
            <li class="nav-item">
                <a class="nav-link btn btn-success btn-sm text-light mx-1 my-1" href="{{route('register')}}" tabindex="-1" aria-disabled="true">Register</a>
              </li>
              @endif
                @endif
          </ul>    
        @endif
        
     
    </div>
    </div>
  </nav>
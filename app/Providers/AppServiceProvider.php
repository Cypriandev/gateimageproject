<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Pagination\Paginator;
use NascentAfrica\Jetstrap\JetstrapFacade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        JetstrapFacade::useCoreUi3();
        Paginator::useBootstrap();
        View::share('appName', 'Gates');
        View::share('tel', '+2348140005666');
        View::share('email', 'icondikenabia@yahoo.com');
        View::share('address', 'somewhere around nowhere');
        View::share('appTitle', 'Get Latest Jobs Update');
        View::share('yr', date("Y"));
        View::share('mnth', date("M"));
        View::share('mth', date("m"));
        View::share('day', date("D"));
    }
}
